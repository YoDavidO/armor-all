angular.module( 'ArmorAll.forgotPass',
  [
    'ui.router'
    'ArmorAll.services'
  ])
.controller 'ForgotPassCtrl', [
  '$scope'
  '$state'
  '$timeout'
  'authServices'
  ( $scope, $state, $timeout, authServices ) ->
    $scope.email = ''
    $scope.invalidEmail = false
    this.sendEmail = ( e )->
      e.preventDefault()

      $scope.uploadingImgs = true
      authServices.sendResetEmail( { email : $scope.email } ).then(
        ( res )->
          $timeout ->
            $state.go 'login'
        ( err )->
          $scope.invalidEmail = true
          $scope.uploadingImgs = false
      )
      return
]
