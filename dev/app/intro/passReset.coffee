angular.module( 'ArmorAll.resetPass',
  [
    'ui.router'
    'ArmorAll.services'
  ])
.controller 'PassResetCtrl', [
  '$scope'
  '$state'
  '$stateParams'
  '$timeout'
  'authServices'
  ( $scope, $state, $stateParams, $timeout, authServices ) ->

    this.submit = ()->
      $scope.uploadingImgs = true
      authServices.resetPassword( $stateParams.email, $stateParams.token, this.pass ).then(
        (res)->
          $timeout ->
            $state.go 'login'
      )
]
