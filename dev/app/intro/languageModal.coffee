langModal = angular.module 'ArmorAll.langModal', [ 'ui.router', 'ArmorAll.services' ]
langModal.controller 'LangModalCtrl', [
    '$rootScope'
    '$state'
    'languageCookie'
    ( $rootScope, $state, languageCookie ) ->

      if( languageCookie.getLanguage() )
        this.closeLangSelect = true
      else
        this.closeLangSelect = false
        languageCookie.setLanguage('english')

      this.setTheLanguage = ( lang ) ->
        languageCookie.setLanguage( lang )
        this.closeLangSelect = true
        hideModal($state)
        $rootScope.$broadcast 'modalLangChange', lang
  ]


hideModal = ($state) ->
  TweenMax.to(
    $( '.langModal' ), .15,
    {
      opacity : 0,
      ease : Quad.easeOut
      onComplete : ->
        $( '.langModal' ).hide()
        $state.go $state.$current.name, {}, { reload : true }
    });