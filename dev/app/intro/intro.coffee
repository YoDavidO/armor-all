angular.module( 'ArmorAll.intro',['ArmorAll.services', 'vcRecaptcha' ] )
  .controller 'IntroCtrl', [
    '$rootScope'
    '$scope'
    'authServices'
    'vcRecaptchaService'
    ( $rootScope, $scope, authServices, vcRecaptchaService )->
      this.multiWrongInput = false

      this.response = null
      this.widgetId = null
      this.key = '6Lco5AUTAAAAAJFjcl09SNZZg9rkv1uvUoBkLE0e'

      this.setResponse = ( response )->
        this.response = response
      this.setWidgetId = ( widgetId )->
        this.widgetId = widgetId

      this.model =
        user : ''
        pass : ''

      $rootScope.$on 'invalidLogin', ->
        $scope.invalidEmail = true
        $scope.uploadingImgs = false

      this.submit = ( e )->
        e.preventDefault()
        $scope.uploadingImgs = true
        authServices.login( this.model.user, this.model.pass )
  ]
