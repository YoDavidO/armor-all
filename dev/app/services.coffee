services = angular.module 'ArmorAll.services', [ 'ngStorage', 'firebase', 'ngFileUpload' ]

services.factory 'ArrayWithLookUp',
  ( $firebaseArray )->
    return $firebaseArray.$extend
      match : ( uid )->
        match = {}
        this.$list.$loaded()
        .then(
          ( recs )->
            i = 0
            while i < recs.length
              if( recs[i].userId == uid )
                match = {
                  rec : recs[i]
                  recIndex : i
                }
                return match
                break
              i++
            return match = { rec : null, recIndex : null }
        )
services.factory 'ArrayWithUpcSearch',
  ( $firebaseArray )->
    return $firebaseArray.$extend
      match : ( upc )->
        i = 0
        while i < this.$list.length
          if( this.$list[i]['UPC Code'] == upc )
            return true
            break
          i++
        return false
services.factory 'ArrayWithDateSearch',
  ( $firebaseArray )->
    return $firebaseArray.$extend
      match : ( winTime )->
        match = {}
        this.$list.$loaded()
        .then(
          ( recs )->
            i = 0
            while i < recs.length
              if( recs[i]['won_time'] == '0000-00-00 00:00:00' && Date.parse(winTime) >= Date.parse(recs[i]['win_time']) )
                match = {
                  rec: recs[i]
                  win: true
                  recIndex: i
                  winTime : winTime.toString()
                }
                return match
                break
              i++
            return match = { rec : null, win : false }
        )
services.factory 'ArrayWithPrizeSearch',
  ( $firebaseArray )->
    return $firebaseArray.$extend
      match : ( prizeId )->
        match = {}
        this.$list.$loaded()
        .then(
          ( recs )->
            i = 0
            while i < recs.length
              if( prizeId == recs[i]['id'] )
                match = {
                  rec : recs[i]
                  recIndex : i
                }
                return match
                break
              i++
            return match = null
        )

services.factory 'MatchEm', ( $sessionStorage )->
  return sift : ( item )->
    i = 0
    while i < $sessionStorage.c.length
      if( $sessionStorage.c[i] == item )
        return true
        break
      i++
    return false

services.service 'languageCookie',[ '$localStorage', ( $localStorage )->
    languageServices = {}
    languageServices.getLanguage = ->
      return $localStorage.language
    languageServices.setLanguage = (lang)->
      $localStorage.language = lang
    return languageServices
  ]

services.service 'authServices', [
  '$rootScope',
  '$state',
  '$timeout',
  '$firebaseAuth',
  '$firebaseObject',
  '$firebaseArray',
  '$sessionStorage',
  'instantWinServices',
  'winnersServices',
  ( $rootScope, $state, $timeout, $firebaseAuth, $firebaseObject, $firebaseArray, $sessionStorage, instantWinServices, winnersServices )->
    authServices = {}
    authServices.user = ''
    authServices.wrongInput = 0
    authServices.showCaptcha = false

    auth = new Firebase 'https://blistering-inferno-4773.firebaseio.com/'
    authServices.authObj = $firebaseAuth auth

    uProfiles = new Firebase 'https://blistering-inferno-4773.firebaseio.com/userProfiles'

    authServices.login = ( user, pass )->
      authServices.authObj.$authWithPassword ({
        email     : user
        password  : pass
      })
      .then(
        ( user )->
          userProfileAcquired = $rootScope.$on 'userProfileAcquired', ->
            $rootScope.logged = true
            winDate = new Date()
            winnersServices.getWinner( $sessionStorage.userPro.userId ).$loaded().then(
              ( snap )->
                if( snap.$value == null )
                  winChecker = instantWinServices.checkForWin( winDate ).then(
                    ( snap )->
                      if( snap.win )
                        snap.rec.winner_id = $sessionStorage.userPro.userId
                        snap.rec.won_time = winDate
                        instantWinServices.updateWin( snap ).then(
                          ( snap )->
                            instantWinServices.getPrize().then(
                              ( snap )->
                                winnerName = $sessionStorage.userPro.firstName + ' ' + $sessionStorage.userPro.lastName.slice( 0, 1 ) + '.'
                                winnerPlace = $sessionStorage.userPro.city + ', ' +  $sessionStorage.userPro.province
                                winnersServices.saveWinner({
                                    name : winnerName
                                    place : winnerPlace
                                    prize : snap.rec.item_eng
                                    prize_fr : snap.rec.item_fr
                                    winner_id : $sessionStorage.userPro.userId
                                } )
                                $timeout ->
                                  userProfileAcquired()
                                  $rootScope.prize = snap.rec.item_eng
                                  $rootScope.prize_fr = snap.rec.item_fr
                                  $rootScope.prize_img = snap.rec.prize_img
                                  $state.go 'winConf'
                            )
                        )
                      else
                        $timeout ->
                          userProfileAcquired()
                          $state.go 'bonusEntry'
                  )
                else
                  $timeout ->
                    userProfileAcquired()
                    $state.go 'bonusEntry'
                )

          authServices.getUserProfile( user.uid )
          upcs = new Firebase 'https://blistering-inferno-4773.firebaseio.com//bonusEntriesUpcs'
          existingUpcs = new $firebaseArray( upcs )
          existingUpcs.$loaded().then(
            ( snap ) ->
              i = 0
              $sessionStorage.c = []
              while i < snap.length
                $sessionStorage.c.push( snap[i]['UPC Code'] )
                i++
              return
          )

        ( err )->
          $rootScope.$broadcast 'invalidLogin'
      )

    authServices.logout = ->
      $rootScope.logged = false
      authServices.authObj.$unauth()
      if( $sessionStorage.userPro )
        $sessionStorage.userPro = null
      $timeout ->
        $state.go 'login'
      return

    authServices.getAuth = ->
      return authServices.authObj.$getAuth

    authServices.getUserProfile = ( uid )->
      uProfiles.orderByChild( "userId" ).equalTo( uid ).on 'value', ( snap )->
        $sessionStorage.proId = Object.keys(snap.val())[0]
        $sessionStorage.userPro = snap.val()[Object.keys(snap.val())[0]];
        $rootScope.$broadcast( 'userProfileAcquired' )
      return

    authServices.updateUserProfile = ( whatToUpdate )->
      ref = new Firebase 'https://blistering-inferno-4773.firebaseio.com/userProfiles/' + $sessionStorage.proId
      proToUpdate = new $firebaseObject( ref ).$loaded().then(
        ( snap )->
          snap[ whatToUpdate ] = $sessionStorage.userPro[ whatToUpdate ]
          snap.$save().then(
            $rootScope.$broadcast 'profileUpdated'
          )
      )
      return

    authServices.sendResetEmail = ( email )->
      return authServices.authObj.$resetPassword( email )
    authServices.resetPassword = ( email, token, newPass ) ->
      return authServices.authObj.$changePassword({ email : email, oldPassword : token, newPassword : newPass } )

    return authServices
  ]

services.service 'regServices', [
  '$rootScope',
  '$state',
  '$timeout',
  '$firebaseAuth',
  '$firebaseArray',
  '$firebaseObject',
  '$sessionStorage',
  'authServices',
  'winnersServices',
  'instantWinServices'
  'upcServices'
  ( $rootScope, $state, $timeout, $firebaseAuth, $firebaseArray, $firebaseObject, $sessionStorage, authServices, winnersServices, instantWinServices, upcServices )->
    regServices = {}

    regServices.createUser = ( registerObj )->
      authServices.authObj.$createUser( registerObj.regInfo )
      .then(
        ( user )->
          authServices.authObj.$authWithPassword( { email : registerObj.regInfo.email, password : registerObj.regInfo.password } )
          .then(
            ( user )->
              authServices.user = user
              registerObj.profInfo.userId = user.uid
              $rootScope.logged = true
              $sessionStorage.c = []
              upcServices.existingUpcs.$loaded().then(
                ( res )->
                  i = 0
                  while i < res.length
                    upcServices.validUpcs.push( res[i]['UPC Code'] )
                    i++
                  $sessionStorage.c = upcServices.validUpcs
              )
              pushUserProfile( registerObj.profInfo )
          )
        ( err )->
          $rootScope.$broadcast 'emailTaken'
      )

    pushUserProfile = ( profObj )->
      regServices.profList = $firebaseArray new Firebase 'https://blistering-inferno-4773.firebaseio.com/userProfiles'
      regServices.profList.$add( profObj ).then(
        ( user )->
          uProfiles = new Firebase 'https://blistering-inferno-4773.firebaseio.com/userProfiles'
          uProfiles.orderByChild( "userId" ).equalTo( profObj.userId ).on 'value', ( snap )->
            $sessionStorage.proId = Object.keys(snap.val())[0]
            $sessionStorage.userPro = snap.val()[Object.keys(snap.val())[0]];
            winDate = new Date()
            winnersServices.getWinner( $sessionStorage.userPro.userId ).$loaded().then(
              ( snap )->
                if( snap.$value == null )
                  winChecker = instantWinServices.checkForWin( winDate ).then(
                    ( res )->
                      if( res.win )
                        res.rec.winner_id = $sessionStorage.userPro.userId
                        res.rec.won_time = winDate
                        instantWinServices.updateWin( res ).then(
                          (res)->
                            instantWinServices.getPrize().then(
                              (res)->
                                instantWinServices.updatePrizes( res ).then(
                                  winnerName = $sessionStorage.userPro.firstName + ' ' + $sessionStorage.userPro.lastName.slice(0,1) + '.'
                                  winnerPlace = $sessionStorage.userPro.city + ', ' + $sessionStorage.userPro.province
                                  winnersServices.saveWinner( {
                                    name : winnerName,
                                    place : winnerPlace,
                                    prize : res.rec.item_eng,
                                    prize_fr : res.rec.item_fr,
                                    winner_id : $sessionStorage.userPro.userId
                                  } )
                                  $timeout ->
                                    $rootScope.prize = res.rec.item_eng
                                    $rootScope.prize_fr = res.rec.item_fr
                                    $rootScope.prize_img = res.rec.prize_img
                                    $state.go 'winConf'
                                )
                            )
                        )
                      else
                        $timeout ->
                          $state.go 'registerConf'
                          return
                  )
                else
                  $timeout ->
                    $state.go 'registerConf'
                    return
            )

            return
          return
      )
      return
    return regServices
  ]

services.service 'imgServices', [ 'Upload', '$state', '$timeout', '$firebaseArray', '$sessionStorage', 'authServices', ( Upload, $state, $timeout, $firebaseArray, $sessionStorage, authServices )->
    imgServices = {}

    ref = new Firebase 'https://blistering-inferno-4773.firebaseio.com/galleryImgs'
    imgServices.imgList = $firebaseArray ref

    ##img uploading##
    imgServices.numOfFiles = 0
    imgServices.uploadImg = ( files )->
      imgServices.i = 0
      imgServices.numOfFiles = files.length

      while imgServices.i < files.length
        if( files[imgServices.i] != "" )
          if( $sessionStorage.userPro.photoCount <= 5 )
            $sessionStorage.userPro.photoCount++
            imgServices.imgUploader( files[imgServices.i] )
        imgServices.i++

    imgServices.imgUploader = ( file )->
      imgServices.upload =
        Upload.upload({
          url: './assets/scripts/upload.php'
          headers:
            'my-header': 'my-header-value'
          method: 'POST'
          file: file
        })
        .progress (evt) ->
          progressPerc = parseInt 100.0 * evt.loaded / evt.total
        .success (data, status, headers, config) ->
          imgServices.imgInfoPush( authServices.getAuth().uid, './assets/images/galleryImages/' + config.file.name )

    imgServices.imgInfoPush = ( uid, imgUrl )->
      imgServices.imgList.$add ( { 'imgUrl' : imgUrl, approved : 'no' } )
        .then(
          ( res )->
            if( imgServices.i >= ( imgServices.numOfFiles - 1 ) )
              authServices.updateUserProfile( 'photoCount' ).then(
                ( res )->
                  $timeout ->
                    $state.go 'bonusConfirmation'
              )

          ( err )->
            console.log( err )
        )

    imgServices.getImages = ->
      imgServices.imgList.$loaded()
      .then(
        ( res )->
          return res
      )

    return imgServices
  ]

services.service 'tipsServices', [
  '$state',
  '$timeout',
  '$firebaseArray',
  '$sessionStorage',
  'authServices',
  ( $state, $timeout, $firebaseArray, $sessionStorage, authServices )->
    tipsServices = {}
    tipsServices.tipIndex = 0
    tipsServices.tipsLength = 0;

    ref = new Firebase 'https://blistering-inferno-4773.firebaseio.com/tipsList'
    tipsServices.tipsList = $firebaseArray ref

    tipsServices.submitTips = ( tips )->
      tipsServices.tipsLength = tips.length

      i = 0
      tmp = []

      while i < tips.length
        if( tips[i].length > 0 )
          tmp.push tips[i]
        i++
      if( ($sessionStorage.userPro.tipsCount + tmp.length) <= 5 )
        $sessionStorage.userPro.tipsCount += tmp.length
        tipsServices.pushTips( tmp )

    tipsServices.pushTips = ( tips ) ->
      i = 0
      while i < tips.length
        tipsServices.tipsList.$add(
          {
            'uid' : $sessionStorage.userPro.firstName + ' ' + $sessionStorage.userPro.lastName.slice( 0, 1 ) + '.',
            'tip' : tips[i],
            'approved' : 'no'
        })
        .then(
            ( res )->
              if( i >= ( tips.length - 1 ) )
                authServices.updateUserProfile( 'tipsCount' )
                .then(
                  ( res ) ->
                    console.log res
                    tipsServices.tipIndex = 0
                    $timeout ->
                      $state.go 'bonusConfirmation'
                )
          )
        i++

    tipsServices.getTips = ->
      return tipsServices.tipsList

    return tipsServices
  ]

services.service 'upcServices', [
  '$firebaseArray'
  ( $firebaseArray )->

    upcServices = {}
    upcServices.validUpcs = []
    ref1 = new Firebase 'https://blistering-inferno-4773.firebaseio.com//bonusEntriesUpcs'
    upcServices.existingUpcs = new $firebaseArray( ref1 )

    return upcServices
  ]

services.service 'instantWinServices', [
    '$rootScope'
    '$state'
    '$timeout'
    '$firebaseArray'
    'ArrayWithDateSearch'
    'ArrayWithPrizeSearch'
    ( $rootScope, $state, $timeout, $firebaseArray, ArrayWithDateSearch, ArrayWithPrizeSearch )->
      instantWinServices = {}


      winDates = new Firebase 'https://blistering-inferno-4773.firebaseio.com/instantWinDates'
      winPrizes = new Firebase 'https://blistering-inferno-4773.firebaseio.com/prizes'

      instantWinServices.checkForWin = ( todaysDate )->
        instantWinServices.winDates = new ArrayWithDateSearch( winDates )
        return instantWinServices.winDates.match( todaysDate )

      instantWinServices.updateWin = ( rec )->
        instantWinServices.prizeId = rec.rec['prize_id']
        instantWinServices.winDates[rec.recIndex].won_time = rec.winTime
        return instantWinServices.winDates.$save( rec.recIndex )

      instantWinServices.getPrize = ()->
        instantWinServices.winPrizes = new ArrayWithPrizeSearch( winPrizes )
        return instantWinServices.winPrizes.match( instantWinServices.prizeId )

      instantWinServices.updatePrizes = ( rec )->
        prizeNum = parseInt( rec.rec['quantity'] )
        if( prizeNum > 0 )
          prizeNum--
        instantWinServices.winPrizes[ rec.recIndex ]['quantity'] = "" + prizeNum
        return instantWinServices.winPrizes.$save( rec.recIndex )

      return instantWinServices
  ]

services.service 'winnersServices', [
  '$firebaseArray'
  '$firebaseObject'
  '$sessionStorage'
  ( $firebaseArray, $firebaseObject )->
    winnersServices = {}

    ref = new Firebase 'https://blistering-inferno-4773.firebaseio.com/winners'
    winnersServices.winnersList = $firebaseArray ref

    winnersServices.getWinner = ( uid )->
      return $firebaseObject( new Firebase 'https://blistering-inferno-4773.firebaseio.com/winners/' + uid )

    winnersServices.getWinners = ()->
      return winnersServices.winnersList

    winnersServices.saveWinner = ( winner )->
      newWinner = $firebaseObject new Firebase 'https://blistering-inferno-4773.firebaseio.com/winners/' + winner.winner_id.split(':')[1]
      newWinner.name = winner.name
      newWinner.place = winner.place
      newWinner.prize_eng = winner.prize
      newWinner.prize_fr = winner.prize_fr
      newWinner.uid = winner.winner_id
      newWinner.$save()
      return

    return winnersServices
  ]
