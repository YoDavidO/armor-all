angular.module( 'ArmorAll.gallery',
  [
    'ArmorAll.services',
  ])
  .controller 'GalleryCtrl', [
    '$scope'
    '$timeout'
    'imgServices'
    ( $scope, $timeout, imgServices ) ->

      $timeout ->
        imgServices.getImages().then(
          ( res )->
            tmp = []

            i = 0

            while i < res.length
              if( res[i].approved == 'yes' )
                tmp.push res[i]
              i++

            $scope.imgUrls = groupIt tmp, 25

            $timeout ->
              carouselInitialize()
              lightboxlInitialize()
        )
  ]

groupIt = ( arr, groupsOf ) ->
  groups = []
  i = 0
  while i < arr.length
    group = arr.slice i, i += groupsOf
    groups.push group
    i++
  return groups

carouselInitialize = ->
  $('.owl-carousel').owlCarousel({
    items:1,
    nav: true,
    autoPlay: 3000,
    margin:10
  });

lightboxlInitialize = ->
  instance = $( 'a[data-imagelightbox="a"]' ).imageLightbox({
    selector:'id="imagelightbox"'
    onStart: ->
      overlayOn()
    onEnd: ->
      overlayOff()
  });

  return

overlayOn = ->
  $('<div id="imagelightbox-overlay"><div id="imagelightbox-close" title="Close"></div></div>').appendTo 'body'
  return

overlayOff = ->
  $('#imagelightbox-overlay').remove()
  return