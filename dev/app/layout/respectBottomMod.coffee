angular.module( 'ArmorAll.bottomMod', [ 'ArmorAll.services' ] )
  .controller 'RespectBottomCtrl', [
    '$scope'
    'tipsServices'
    ( $scope, tipsServices ) ->

      tipsServices.getTips().$loaded().then(
        (res)->
          $scope.tips = []

          i = 0
          while i < res.length
            if( res[i].approved == 'yes' )
              $scope.tips.push res[i]
            i++

          $scope.$broadcast( 'rebuild:me' )
      )
  ]