angular.module( 'ArmorAll.layout', ['ui.router', 'ngStorage', 'ngScrollbar', 'ArmorAll.services' ])
.controller 'LayoutCtrl', [
  '$rootScope'
  '$scope'
  '$state'
  '$timeout'
  '$sessionStorage'
  'languageCookie'
  'authServices'
  ( $rootScope, $scope, $state, $timeout, $sessionStorage, languageCookie, authServices ) ->
    this.openMobileNav = mobileNavOpener
    $scope.headerFooterLang = true
    $scope.isLogged = { notlogged : false }
    $scope.loginString = 'Login'
    $scope.logoutString = 'Logout'

    $scope.loginText = ()->
      if( $sessionStorage.userPro )
        $scope.isLogged = true
        return $scope.logoutString
      else
        $scope.isLogged = false
        return $scope.loginString

    $scope.loginText()

    $rootScope.$on 'loggedIn', ( e, d ) ->
      $scope.loginText()

    $rootScope.$on 'modalLangChange', ( lang )->

      if( languageCookie.getLanguage() == 'english' )
        $timeout ->
          $scope.headerFooterLang = true
      else
        $timeout ->
          $scope.headerFooterLang = false

    this.setTheLanguage = ()->
      if ( languageCookie.getLanguage() == 'english' )
        languageCookie.setLanguage( 'french' )
        $scope.loginString = 'Se Connecter'
        $scope.logoutString = 'Se Deconnecter'
        $scope.headerFooterLang = false
      else
        $scope.loginString = 'Login'
        $scope.logoutString = 'Logout'
        languageCookie.setLanguage( 'english' )
        $scope.headerFooterLang = true
      $timeout ->
        $rootScope.$broadcast 'changeLanguage'
        $state.go $state.$current.name, {}, { reload : true }

    this.currentLanguage = ->
      return languageCookie.getLanguage()

    this.logMe = ->
      if( authServices.getAuth() )
        authServices.logout()
      $scope.loginText()

    this.logout = ( e )->
      e.preventDefault()
      authServices.logout()

    if( languageCookie.getLanguage() )
      if( languageCookie.getLanguage() == 'french' )
        $scope.headerFooterLang = false
      else
        $scope.headerFooterLang = true
    else
      this.languageText = 'Francais'

    return

]

mobileNavOpener = ->
  if( $( '.masthead' ).hasClass 'open-nav' )
    $( '.masthead' ).removeClass 'open-nav'
    $( '.navigation' ).removeClass 'open-nav'
  else
    $( '.masthead' ).addClass 'open-nav'
    $( '.navigation' ).addClass 'open-nav'

  return true;
