angular.module( 'ArmorAll.help',
  [
    'ngRoute'
  ])
.config([
    '$routeProvider'
    ( $routeProvider ) ->
      $routeProvider.when '/help',
        templateUrl : './assets/partials/help.html'
        controller  : 'HelpCtrl'
  ])
.controller 'HelpCtrl', [
  '$scope'
  ( $scope ) ->
    console.log 'This is the ArmorAll.help page!'
]


#$("#contactform").submit(function(){
#  var str = $(this).serialize();
#  $.ajax({
#      type: "POST",
#      url: "php/send.php",
#      data: str,
#      success: function(msg)
#    {
#    $("#contactform").slideUp("fast");
#    result = '<div class="formstatusok">Your message has been sent.<br/> Thank you!</div>';
#    $("#formstatus").html(result);
#
#
#}
#
#});
#return false;
#});