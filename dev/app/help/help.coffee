angular.module( 'ArmorAll.help',
  [

  ])
.controller 'HelpCtrl', [
  '$scope'
  '$http'
  '$timeout'
  ( $scope, $http, $timeout ) ->
    $scope.formData = {}
    this.processForm = ->
      console.log $scope.formData
      $http(
        method: 'POST'
        url: 'assets/scripts/send_form.php'
        data: $.param($scope.formData)
        headers: 'Content-Type': 'application/x-www-form-urlencoded'
      )
      .success (data) ->
        console.log data
        if !data.success
          this.errorName = data.errors.name
          this.errorEmail = data.errors.email
        else
          console.log 'sent!'
          this.message = data.message
          $timeout ->
            $scope.showThankyou = "Your message has been sent. Thank You!"
            $scope.showThankyouFR = "Votre message a été envoyé. Merci!"

]
