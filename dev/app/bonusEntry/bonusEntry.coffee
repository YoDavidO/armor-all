angular.module( 'ArmorAll.bonusEntry',[
  'ui.router'
  'ngStorage'
  'ArmorAll.services'
])
.controller( 'BonusEntryCtrl', [
    '$scope'
    ( $scope ) ->
      return
  ])
.controller( 'PhotoEntryCtrl', [
    '$rootScope'
    '$scope'
    '$sessionStorage'
    'imgServices'
    ( $rootScope, $scope, $sessionStorage, imgServices ) ->
      $scope.uploadingImgs = false

      this.files = [ '', '', '', '', '' ]
      this.currentPhotoEntryCount = $sessionStorage.userPro.photoCount

      this.openBrowse = ( which ) ->
        openFileSystem( which )

      this.onFileSelect = ( file, index ) ->
        if( file.length > 0 )
          this.files[index] = file[0]

      this.uploadImages = ()->
        $scope.uploadingImgs = true
        if( this.currentPhotoEntryCount < 5 )
          imgServices.uploadImg( this.files )

      $rootScope.$broadcast( 'rebuildScroller' )

      $scope.disableMe = [ false, false, false, false, false ]

      if( this.currentPhotoEntryCount > 0 )
        i = 4
        j = i - this.currentPhotoEntryCount
        while i > j
          $scope.disableMe[i] = true
          i--

      return
  ])
.controller( 'TipsEntryCtrl', [
    '$scope'
    '$sessionStorage'
    'tipsServices'
    ( $scope, $sessionStorage, tipsServices ) ->
      this.tips = [ '', '', '', '' ,'' ]

      this.currentTipsEntryCount = $sessionStorage.userPro.tipsCount

      this.submitTips = ->
        $scope.uploadingImgs = true
        if( this.currentTipsEntryCount < 5 )
          tipsServices.submitTips( this.tips )


      $scope.disableMe = [ false, false, false, false, false ]

      if( this.currentTipsEntryCount > 0 )
        i = 4
        j = i - this.currentTipsEntryCount
        while i > j
          $scope.disableMe[i] = true
          i--

      return
  ])
.controller( 'UpcEntryCtrl', [
    '$rootScope'
    '$scope'
    '$sessionStorage'
    '$timeout'
    '$state'
    'authServices'
    'MatchEm'
    ( $rootScope, $scope, $sessionStorage, $timeout, $state, authServices, MatchEm ) ->

      upcMatcher = MatchEm

      this.currentUpcEntryCount = $sessionStorage.userPro.upcCount

      this.upcsAdded = 5
      this.upcInput = [
        {
          upc : ''
          invalid : false
        },
        {
          upc : ''
          invalid : false
        },
        {
          upc : ''
          invalid : false
        },
        {
          upc : ''
          invalid : false
        },
        {
          upc : ''
          invalid : false
        }
      ]

      this.compareUpc = ->
        profUpdated = $rootScope.$on 'profileUpdated', ->
          $timeout ->
            profUpdated()
            $state.go 'bonusConfirmation'

        i = 0
        this.upcsAdded = 0
        while i < this.upcInput.length
          if( this.upcInput[i].upc.length > 0)

            formattedUpc = this.upcInput[i].upc
            if( formattedUpc.charAt( 0 ) == '0' )
              formattedUpc = this.upcInput[i].upc.substr( 1, this.upcInput[i].upc.length )
            if( formattedUpc.search( ' ' ) && formattedUpc.split( ' ' ).length > 1)
              formattedUpc = splitUpc[0].trim() +  splitUpc[1].trim()

            if( upcMatcher.sift( formattedUpc ) )
              this.upcInput[i].invalid = false
              this.upcsAdded++
            else
              this.upcInput[i].invalid = true
              break
          i++

        if( this.upcsAdded > 0 && ( this.currentUpcEntryCount + this.upcsAdded ) <= 5 )
          $scope.uploadingImgs = true
          $sessionStorage.userPro.upcCount = ( this.currentUpcEntryCount + this.upcsAdded )
          authServices.updateUserProfile( 'upcCount' )
        return

      $scope.disableMe = [ false, false, false, false, false ]
      if( this.currentUpcEntryCount > 0 )
        i = 4
        j = i - this.currentUpcEntryCount
        while i > j
          $scope.disableMe[i] = true
          i--
  ])

openFileSystem = ( which ) ->
  $( '.browser' + which ).trigger('click');
  return
