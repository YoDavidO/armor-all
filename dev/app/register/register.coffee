angular.module( 'ArmorAll.register',[ 'ui.router', 'ArmorAll.services', 'vcRecaptcha', 'validation.match' ] )
  .controller 'RegisterCtrl', [
    '$rootScope'
    '$scope'
    '$state'
    '$http'
    'regServices'
    'vcRecaptchaService'
    ( $rootScope, $scope, $state, $http, regServices, vcRecaptchaService ) ->
      this.response = null
      this.widgetId = null
      this.key = '6Lco5AUTAAAAAJFjcl09SNZZg9rkv1uvUoBkLE0e'
      this.recap = 'notanswered'
      this.setResponse = ( response )->
        this.response = response
      this.setWidgetId = ( widgetId )->
        this.widgetId = widgetId

      $scope.registerInfo = {
          email : ''
          emailConf : ''
          pass : ''
          passConf : ''
          prefix : ''
          firstName : ''
          lastName : ''
          address : ''
          suite : ''
          city : ''
          province : ''
          postalCode : ''
          phone : ''
          birthYr : ''
          terms : ''
          newsStp : ''
          newsArmor : ''
          MiniNews : ''
          CanadianNews : ''
          userId : ''
          userKey : ''
          captcha : ''
          testAnswer : ''
          real : '5'
      }

      $scope.emailTaken = false

      $rootScope.$on 'emailTaken', ->
        $scope.uploadingImgs = false
        $scope.emailTaken = true

      this.submit = ( e ) ->
        e.preventDefault()
        if( this.widgetId )
          vcRecaptchaService.reload( this.widgetId )

        $scope.registerInfoSplit = {
          regInfo :
            email : this.registerInfo.email
            password : this.registerInfo.pass
          profInfo :
            email : this.registerInfo.email
            prefix : this.registerInfo.prefix || "none"
            firstName : this.registerInfo.firstName
            lastName : this.registerInfo.lastName
            address : this.registerInfo.address
            suite : this.registerInfo.suite || "none"
            city : this.registerInfo.city
            province : this.registerInfo.province
            postalCode : this.registerInfo.postalCode
            phone : this.registerInfo.phone
            birthYr : this.registerInfo.birthYr
            terms : this.registerInfo.terms
            newsStp : this.registerInfo.newsStp || false
            newsArmor : this.registerInfo.newsArmor || false
            MiniNews : this.registerInfo.MiniNews || false
            CanadianNews : this.registerInfo.CanadianNews || false
            photoCount : 0
            tipsCount : 0
            upcCount : 0
            entryCount : 0
            userId : this.registerInfo.userId
            userKey : this.registerInfo.userKey || 'tbd'
        }
        if( this.registerInfo.testAnswer == '5' )
          if( this.recap == 'notanswered' )
            $scope.uploadingImgs = true
            $http.post( './assets/scripts/recaptcha.php', { 'response' : this.response } )
            .success ( data, status, headers, config )->
              this.recap = 'answered'
              if( data == 'fail' )
                console.log( 'please retry captcha' )
              else
                regServices.createUser( $scope.registerInfoSplit )
            .error ( data, status, headers, config )->
              console.log 'error - ' + status
          else
            $scope.uploadingImgs = true
            regServices.createUser( $scope.registerInfoSplit )
  ]
