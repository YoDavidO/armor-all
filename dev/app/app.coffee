app = angular.module( 'ArmorAll',
  [
    'ui.router'
    'ngCookies'
    'ngStorage'
    'vcRecaptcha'
    'validation.match'
    'ArmorAll.services'
    'ArmorAll.directives'
    'ArmorAll.layout'
    'ArmorAll.bottomMod'
    'ArmorAll.langModal'
    'ArmorAll.intro'
    'ArmorAll.register'
    'ArmorAll.bonusEntry'
    'ArmorAll.gallery'
    'ArmorAll.resetPass'
    'ArmorAll.forgotPass'
    'ArmorAll.help'
    'ArmorAll.winners'
  ])
  .config([
      '$stateProvider'
      '$urlRouterProvider'
      ( $stateProvider, $urlRouterProvider ) ->

        $urlRouterProvider.otherwise ( $injector, $location )->
          $state = $injector.get( '$state' )
          $state.go "login"

        $stateProvider.state( 'login',{
          url : '/login'
          views :
            'english' :
              templateUrl : './assets/partials/intro.html'
            'french' :
              templateUrl : './assets/partials/intro_fr.html'
          data :
            needsAuth : false
        })
        .state( 'forgotPass',{
          url : '/forgot-password'
          views :
            'english' :
              templateUrl : './assets/partials/forgotPassword.html'
            'french' :
              templateUrl : './assets/partials/forgotPassword_fr.html'
          data :
            needsAuth : false
        })
        .state( 'passReset',{
          url : '/reset-password/:email/:token'
          views :
            'english' :
              templateUrl : './assets/partials/passwordReset.html'
            'french' :
              templateUrl : './assets/partials/passwordReset_fr.html'
          data :
            needsAuth : false
        })
        .state( 'register',{
            url : '/register'
            views :
              'english' :
                templateUrl : './assets/partials/register.html'
              'french' :
                templateUrl : './assets/partials/register_fr.html'
            data :
              needsAuth : false
          })
        .state( 'registerConf',{
            url : '/register-confirmation'
            views :
              'english' :
                templateUrl : './assets/partials/confirmation.html'
              'french' :
                templateUrl : './assets/partials/confirmation_fr.html'
            data :
              needsAuth : false
          })
        .state( 'winConf',{
            url : '/win-confirmation'
            views :
              'english' :
                templateUrl : './assets/partials/instantwin.html'
                controller : ( $scope, $rootScope )->
                  if( $rootScope.prize )
                    $scope.prize = $rootScope.prize
                    $scope.prize_img = $rootScope.prize_img
              'french' :
                templateUrl : './assets/partials/instantwin_fr.html'
                controller : ( $scope, $rootScope )->
                  if( $rootScope.prize_fr )
                    $scope.prize = $rootScope.prize_fr
                    $scope.prize_img = $rootScope.prize_img
            data :
              needsAuth : false
          })
        .state( 'howToPlay',{
            url : '/how-to-play'
            views :
              'english' :
                templateUrl : './assets/partials/howToPlay.html'
              'french' :
                templateUrl : './assets/partials/howToPlay_fr.html'
            data :
              needsAuth : false
          })
        .state( 'bonusEntry',{
            url : '/bonus-entry'
            views :
              'english' :
                templateUrl : './assets/partials/bonusEntry.html'
              'french' :
                templateUrl : './assets/partials/bonusEntry_fr.html'
            data :
              needsAuth : false
          })
        .state( 'upcEntry',{
            url : '/upc-entry'
            views :
              'english' :
                templateUrl : './assets/partials/upcEntry.html'
              'french' :
                templateUrl : './assets/partials/upcEntry_fr.html'
            data :
              needsAuth : true
          })
        .state( 'tipsEntry',{
            url : '/tips-entry'
            views :
              'english' :
                templateUrl : './assets/partials/tipsEntry.html'
              'french' :
                templateUrl : './assets/partials/tipsEntry_fr.html'
            data :
              needsAuth : true
          })
        .state( 'photoEntry',{
            url : '/photo-entry'
            views :
              'english' :
                templateUrl : './assets/partials/photoEntry.html'
              'french' :
                templateUrl : './assets/partials/photoEntry_fr.html'
            data :
              needsAuth : true
          })
        .state( 'bonusConfirmation',{
            url : '/bonus-entry-confirmation'
            views :
              'english' :
                templateUrl : './assets/partials/bonusEntryConf.html'
              'french' :
                templateUrl : './assets/partials/bonusEntryConf_fr.html'
            data :
              needsAuth : true
          })
        .state( 'gallery',{
            url : '/gallery'
            views :
              'english' :
                templateUrl : './assets/partials/gallery.html'
              'french' :
                templateUrl : './assets/partials/gallery_fr.html'
            data :
              needsAuth : false
          })
        .state( 'prizeDetails',{
            url : '/prize-details'
            views :
              'english' :
                templateUrl : './assets/partials/prizeDetails.html'
              'french' :
                templateUrl : './assets/partials/prizeDetails_fr.html'
            data :
              needsAuth : false
          })
        .state( 'newProducts',{
            url : '/new-products'
            views :
              'english' :
                templateUrl : './assets/partials/newProducts.html'
              'french' :
                templateUrl : './assets/partials/newProducts_fr.html'
            data :
              needsAuth : false
          })
        .state( 'winners',{
            url : '/winners'
            views :
              'english' :
                templateUrl : './assets/partials/winners.html'
              'french' :
                templateUrl : './assets/partials/winners_fr.html'
            data :
              needsAuth : false
          })
        .state( 'rulesAndRegs',{
            url : '/rules-and-regulations'
            views :
              'english' :
                templateUrl : './assets/partials/rulesAndRegs.html'
              'french' :
                templateUrl : './assets/partials/rulesAndRegs_fr.html'
            data :
              needsAuth : false
          })
        .state( 'privacyPolicy',{
            url : '/privacy-policy'
            views :
              'english' :
                templateUrl : './assets/partials/privacyPolicy.html'
              'french' :
                templateUrl : './assets/partials/privacyPolicy_fr.html'
            data :
              needsAuth : false
          })
        .state( 'termsConditions',{
            url : '/terms-conditions'
            views :
              'english' :
                templateUrl : './assets/partials/termsConditions.html'
              'french' :
                templateUrl : './assets/partials/termsConditions_fr.html'
            data :
              needsAuth : false
          })
        .state( 'help',{
            url : '/help'
            views :
              'english' :
                templateUrl : './assets/partials/help.html'
              'french' :
                templateUrl : './assets/partials/help_fr.html'
            data :
              needsAuth : false
          })

        .state( 'contactUs',{
            url : '/contact-us'
            views :
              'english' :
                templateUrl : './assets/partials/contactUs.html'
              'french' :
                templateUrl : './assets/partials/contactUs_fr.html'
            data :
              needsAuth : false
          })

    ])
    .run( ( $rootScope, $state, $sessionStorage, authServices )->
      $rootScope.$on '$stateChangeStart', ( e, to )->
        if( to.data.needsAuth && $sessionStorage.userPro )
          return
        else
          if( to.data.needsAuth )
            if( to.name == 'login' )
              $rootScope.logged = false
              return
            else
              e.preventDefault()
              $rootScope.logged = false
              $state.go 'login'
          else
            return
    )
