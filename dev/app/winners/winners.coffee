angular.module( 'ArmorAll.winners',
  [
    'ArmorAll.services'
  ])
  .controller 'WinnersCtrl', [
    '$scope'
    'winnersServices'
    ( $scope, winnersServices ) ->
      winnersServices.getWinners().$loaded().then(
        ( res )->
          $scope.winners = res
      )
  ]

