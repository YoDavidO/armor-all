directives = angular.module 'ArmorAll.directives', []

directives.directive 'uiLanguage', ->
  return {
    restrict : 'E'
    link : ( scope, element, attrs )->
      attrs.$observe 'language', ( lang )->
        scope.language = lang
    template: '<div class="uiViewPort" ui-view="{{language}}"></div>'
  }