var gulp = require( 'gulp' );
var gutil = require( 'gulp-util' );
var concat = require( 'gulp-concat' );
var coffee = require( 'gulp-coffee' );
var stylus = require( 'gulp-stylus' );
var uglify = require( 'gulp-uglify' );
var minifyCss= require( 'gulp-minify-css' );
var bower = require( 'gulp-bower' );

gulp.task( 'coffee', function(){
    gulp.src( [ './dev/app/app.coffee', './dev/app/**/*.coffee' ] )
        .pipe( coffee() )
        .pipe( concat( 'site.min.js' ) )
        .pipe( gulp.dest( './public/assets/scripts' ) )
});

gulp.task( 'vendor', function(){
    gulp.src( [
        './bower_components/jquery/dist/jquery.js',
        './bower_components/gsap/src/uncompressed/TweenMax.js',
        './bower_components/angular/angular.js',
        './bower_components/angular-ui-router/release/angular-ui-router.js',
        './bower_components/angular-cookies/angular-cookies.js',
        './bower_components/angular-recaptcha/release/angular-recaptcha.js',
        './bower_components/angular-validation-match/dist/angular-input-match.js',
        './bower_components/firebase/firebase.js',
        './bower_components/ng-file-upload/ng-file-upload-shim.js',
        './bower_components/ng-file-upload/ng-file-upload.js',
        './bower_components/angularfire/dist/angularfire.js',
        './bower_components/ngstorage/ngStorage.js',
        './bower_components/ng-scrollbar/dist/ng-scrollbar.js',
        './bower_components/owl.carousel.2.0.0-beta.2.4/owl.carousel.js',
        './bower_components/image-lightbox/imagelightbox.min.js'] )
        .pipe( concat( 'vendor.min.js' ) )
        .pipe( gulp.dest( './public/assets/scripts' ) )
});

gulp.task( 'stylus', function(){
    gulp.src( './dev/app/style.styl' )
        .pipe( stylus() )
        .pipe( minifyCss() )
        .pipe( concat( 'site.min.css' ) )
        .pipe( gulp.dest( './public/assets/styles' ) )
});

gulp.task( 'partials', function(){
    gulp.src( [
        './dev/app/layout/*.html',
        './dev/app/bonusEntry/*.html',
        './dev/app/gallery/*.html',
        './dev/app/howToPlay/*.html',
        './dev/app/newProducts/*.html',
        './dev/app/prizeDetails/*.html',
        './dev/app/register/*.html',
        './dev/app/winners/*.html',
        './dev/app/help/*.html',
        './dev/app/rulesAndRegs/*.html',
        './dev/app/intro/*.html' ] )
        .pipe( gulp.dest( './public/assets/partials' ) )
});

gulp.task( 'index', function(){
    gulp.src( './dev/index.html' )
        .pipe( gulp.dest( './public' ) )
});

gulp.task( 'watch', function(){
    gulp.watch( './dev/app/**/*.coffee', [ 'coffee' ] );
    gulp.watch( './dev/app/**/*.styl', [ 'stylus' ] );
    gulp.watch( './dev/app/**/*.html', ['partials'] );
    gulp.watch( './dev/index.html', [ 'index' ] );
});

gulp.task( 'default', [ 'coffee', 'stylus', 'vendor', 'partials', 'index', 'watch' ] );
